## 0.2.7 (2025-03-03)

### removed (1 change)

- [[SIMPL-001](https://jira.simplprogramme.eu/browse/SIMPL-001) Removed small...](https://code.europa.eu/simpl/simpl-open/Operations/pipeline-test-mvn/-/commit/dd0226c9effde5b3501ac24a8197263c17edcefd) ([merge request](https://code.europa.eu/simpl/simpl-open/Operations/pipeline-test-mvn/-/merge_requests/51))

