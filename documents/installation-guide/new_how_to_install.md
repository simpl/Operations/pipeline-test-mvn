# Installation Guide

|![European Commission](https://european-union.europa.eu/sites/default/files/styles/oe_theme_medium_no_crop/public/2021-02/european-commission_0.jpg)                | EUROPEAN COMMISSION <br/><br/> DIRECTORATE-GENERAL FOR COMMUNICATIONS NETWORKS, CONTENT AND TECHNOLOGY <br/> <br/>Future Networks <br/> Cloud and Software |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------|


| D1.6.1 Simpl-Open Installation Guide |
|--------------------------------------|
| **SC1**                              |


## Document Control Information

| Settings         | Value                                  |
|------------------|----------------------------------------|
| Document Title:  | D1.6.1 Simpl-Open Installation Guide   |
| Project Title:   | SC1                                    |
| Document Author: | Sovereign-X                            |
| Doc. Version:    | 0.1                                    |
| Sensitivity:     | Limited                                |
| Date:            | 28 Jun 2024                            |

## Document Approver(s) and Reviewer(s):

> **📘️** *NOTE: All Approvers are required. Records of each approver must be maintained. All Reviewers in the list are considered required unless explicitly listed as Optional.*

| Name | Role | Action               | Date |
|------|------|----------------------|------|
|      |      | _<Approve / Review>_ |      |

## Document history:

The Document Author is authorised to make the following types of changes to the document without requiring that the document be re-approved:

- Editorial, formatting, and spelling
- Clarification

<br>
To request a change to this document, contact the Document Author or Owner.

Another list:
  1. one
  2. two

Changes to this document are summarized in the following table in reverse chronological order (latest version first).

| Revision | Date        | Created by  | Short Description of Changes |
|----------|-------------|-------------|------------------------------|
| 0.1      | 28 Jun 2024 | Sovereign-X | SfR M6 first iteration       |


<!-- TOC -->
* [Installation Guide](#installation-guide)
  * [Document Control Information](#document-control-information)
  * [Document Approver(s) and Reviewer(s):](#document-approvers-and-reviewers)
  * [Document history:](#document-history)
* [Overview Simpl-Open product](#overview-simpl-open-product)
* [Simpl-Open agents architecture overview](#simpl-open-agents-architecture-overview)
* [Simpl-Open Agents Deployment](#simpl-open-agents-deployment)
  * [Dataspace Installation Overview](#dataspace-installation-overview)
  * [Pre-requisites](#pre-requisites)
    * [Hardware/Software requirements](#hardwaresoftware-requirements)
      * [Hardware requirements](#hardware-requirements)
      * [Middleware](#middleware)
      * [Platform Requirements](#platform-requirements)
    * [Tools requirements](#tools-requirements)
    * [Supported Configuration](#supported-configuration)
  * [Version changes list](#version-changes-list)
  * [Governance agent installation](#governance-agent-installation)
    * [Installation description](#installation-description)
    * [Artefacts links](#artefacts-links)
    * [Troubleshooting](#troubleshooting)
  * [Data provider agent installation](#data-provider-agent-installation)
    * [Installation description](#installation-description-1)
    * [Artefacts links](#artefacts-links-1)
    * [Troubleshooting](#troubleshooting-1)
  * [Application provider agent installation](#application-provider-agent-installation)
    * [Installation description](#installation-description-2)
    * [Artefacts links](#artefacts-links-2)
    * [Troubleshooting](#troubleshooting-2)
  * [Infrastructure provider agent installation](#infrastructure-provider-agent-installation)
    * [Installation description](#installation-description-3)
    * [Artefacts links](#artefacts-links-3)
    * [Troubleshooting](#troubleshooting-3)
  * [Data consumer agent installation](#data-consumer-agent-installation)
    * [Installation description](#installation-description-4)
    * [Artefacts links](#artefacts-links-4)
    * [Troubleshooting](#troubleshooting-4)
  * [Deployment validation and Testing topics](#deployment-validation-and-testing-topics)
    * [Testing artefacts](#testing-artefacts)
    * [FAQ](#faq)
<!-- TOC -->

# Overview Simpl-Open product
_Insert here the overview from Architecture doc overview_

# Simpl-Open agents architecture overview

![Components arhitecture](images/arhitecture.png)


![Integration tier](images/integration.png)


# Simpl-Open Agents Deployment

## Dataspace Installation Overview

Simpl-Open Agents high-level installation steps:

  1. Install the Governance Authority agent.
  2. Configure EJBCA component to generate the needed certificates for the Governance Authority TLS gateway communications to work with any other participants.
  3. Proceed with the Data Provider on-boarding process using the Governance Authority On-boarding front-end.
  4. At the end of the process, the Data provider certificates can be downloaded and are to be used later for the configuration of the Data provider agent.
  5. Install a Data Provider agent.
  6. Check Data Provider Catalogue front-end can connect to the Governance Authority Catalogue for a search.
  7. Redo the steps 3. to 7. for the Data Consumer.

## Pre-requisites

### Hardware/Software requirements

#### Hardware requirements

Supported OS - any of the supported versions of Windows, macOS or Linux

Governance Authority:

  - CPU - tbd 
  - RAM - tbd 
  - Memory - tbd 
  - Node size - tbd 

Data Provider:

  - CPU - tbd 
  - RAM - tbd 
  - Memory - tbd 
  - Node size - tbd

Application Provider:

  - CPU - tbd
  - RAM - tbd
  - Memory - tbd
  - Node size - tbd

Infrastructure Provider:

  - CPU - tbd
  - RAM - tbd
  - Memory - tbd
  - Node size - tbd

Consumer:

  - CPU - tbd
  - RAM - tbd
  - Memory - tbd
  - Node size - tbd

#### Middleware

#### Platform Requirements
  - "Vanilla" Kubernetes

### Tools requirements

| Tool                |        Version        | Description                                                                                                                                                                                  |
|---------------------|:---------------------:|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Git                 |   2.47.x or higher    | Version control system for tracking and managing changes to agents https://git-scm.com/downloads                                                                                             |
| Helm                |   3.14.x or higher    | Package Manager for Kubernetes https://github.com/helm/helm/releases                                                                                                                         |
| Kubernetes Cluster  |    1.29.x or newer    | Other version *might* work but tests were performed using 1.29.x version                                                                                                                     |
| Argo CD             |    2.11.x or newer    | Used as GitOps tool . App of apps concept. <br/> Other version *might* work but tests were performed using 2.11.x version. <br/> Image used: `quay.io/argoproj/argocd:v2.11.3`               |
| DNS sub-domain name |          N/A          | This domain will be used to address all services of the agent. <br/> example: `*.authority1.int.simpl-europe.eu`                                                                             |
| nginx-ingress       |    1.10.x or newer    | Used as ingress controller. <br/> Other version *might* work but tests were performed using 1.10.x version. <br/> Image used: `registry.k8s.io/ingress-nginx/controller:v1.10.0`             |
| cert-manager        |    1.15.x or newer    | Used for automatic cert management. <br/> Other version *might* work but tests were performed using 1.15.x version. <br/> Image used: `quay.io/jetstack/cert-manager-controller::v1.15.3`    |
| Hashicorp Vault     |    1.17.x or newer    | Other version *might* work but tests were performed using 1.17.x version. <br/> Image used: `hashicorp/vault:1.17.2`                                                                         |
| nfs-provisioner     |    4.0.x or newer     | Backend for *Read/Write many* volumes. <br/> Other version *might* work but tests were performed using 4.0.x version. <br/> Image used: `registry.k8s.io/sig-storage/nfs-provisioner:v4.0.8` |

### Supported Configuration

## Version changes list

| Required artifact             | Version | Link | Summary |
|-------------------------------|---------|------|---------|
| Governance authority          |         |      |         |
| Data Consumer agent           |         |      |         |
| Data provider agent           |         |      |         |
| Infrastructure provider agent |         |      |         |
| Application provider agent    |         |      |         |

## Governance agent installation

### Installation description

### Artefacts links

### Troubleshooting

## Data provider agent installation

### Installation description

### Artefacts links

### Troubleshooting

## Application provider agent installation

### Installation description

### Artefacts links

### Troubleshooting

## Infrastructure provider agent installation

### Installation description

### Artefacts links

### Troubleshooting

## Data consumer agent installation

### Installation description

### Artefacts links

### Troubleshooting

## Deployment validation and Testing topics

### Testing artefacts

Link to Integration team manual test cases export form Jira and link to automated test case suite in GitLab artefact repositories

### FAQ
Issues FAQ

Troubleshooting tools, test tools and case to support troubleshooting

Known issues list
