## 0.2.6 (2025-03-03)

### fixed (1 change)

- [[SIMPL-999](https://jira.simplprogramme.eu/browse/SIMPL-999) Fixed something.](https://code.europa.eu/simpl/simpl-open/Operations/pipeline-test-mvn/-/commit/0fb260225cbb875cb7a122ae023837ce86f5e4bd) ([merge request](https://code.europa.eu/simpl/simpl-open/Operations/pipeline-test-mvn/-/merge_requests/50))


## 0.2.5 (2025-03-03)

### added (1 change)

- [[SIMPL-1234](https://jira.simplprogramme.eu/browse/SIMPL-1234) Added new...](https://code.europa.eu/simpl/simpl-open/Operations/pipeline-test-mvn/-/commit/2daaeb7e3489031eab689ad54bac4872a6e91ae8) ([merge request](https://code.europa.eu/simpl/simpl-open/Operations/pipeline-test-mvn/-/merge_requests/49))


## 0.2.4 (2025-03-03)

### changed (2 changes)

- [[SIMPL-10068] Upgraded the release version number to 0.2.4](https://code.europa.eu/simpl/simpl-open/Operations/pipeline-test-mvn/-/commit/9663033ec4cd9e0edd4e7ccf4e308c59517caf84) ([merge request](https://code.europa.eu/simpl/simpl-open/Operations/pipeline-test-mvn/-/merge_requests/48))
- [[SIMPL-10414](https://jira.simplprogramme.eu/browse/SIMPL-10414) Test on develop branch.](https://code.europa.eu/simpl/simpl-open/Operations/pipeline-test-mvn/-/commit/460ba33a0966d7b28d1b1505389b56d1d2e86b8d) ([merge request](https://code.europa.eu/simpl/simpl-open/Operations/pipeline-test-mvn/-/merge_requests/47))

### fixed (1 change)

- [[SIMPL-10436](https://jira.simplprogramme.eu/browse/SIMPL-10436) Another test on feature branch.](https://code.europa.eu/simpl/simpl-open/Operations/pipeline-test-mvn/-/commit/1d0e66e791ea5ef95aff7f3c585dcec35b801468) ([merge request](https://code.europa.eu/simpl/simpl-open/Operations/pipeline-test-mvn/-/merge_requests/47))

