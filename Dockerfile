FROM openjdk:8-jre-alpine

COPY /usr/src/app/target/*.jar /app.jar

RUN adduser -D dockeruser
USER dockeruser

EXPOSE 8080

ENTRYPOINT ["java"]
CMD ["-jar", "/app.jar"]
